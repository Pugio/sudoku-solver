debug();

function debug(){

	// Filling in some stuff to make it easier to Debug
	/* Vertical Line */
	var c00 = document.getElementById("c00").childNodes[0];
	var c01 = document.getElementById("c01").childNodes[0];
	var c02 = document.getElementById("c02").childNodes[0];
	var c03 = document.getElementById("c03").childNodes[0];
	var c04 = document.getElementById("c04").childNodes[0];
	var c05 = document.getElementById("c05").childNodes[0];
	var c06 = document.getElementById("c06").childNodes[0];
	var c07 = document.getElementById("c07").childNodes[0];

	c00.value = 1;
	c01.value = 2;
	c02.value = 3;
	c03.value = 4;
	c04.value = 5;
	c05.value = 6;
	c06.value = 7;
	c07.value = 8;

	/* Horizontal Line */
	var c10 = document.getElementById("c10").childNodes[0];
	var c20 = document.getElementById("c20").childNodes[0];
	var c30 = document.getElementById("c30").childNodes[0];
	var c40 = document.getElementById("c40").childNodes[0];
	var c50 = document.getElementById("c50").childNodes[0];
	var c60 = document.getElementById("c60").childNodes[0];
	var c70 = document.getElementById("c70").childNodes[0];

	c10.value = 4;
	c20.value = 5;
	c30.value = 2;
	c40.value = 3;
	c50.value = 6;
	c60.value = 7;
	c70.value = 8;

	/* Secound Vertical Line */
	var c11 = document.getElementById("c11").childNodes[0];
	var c12 = document.getElementById("c12").childNodes[0];
	var c13 = document.getElementById("c13").childNodes[0];
	var c14 = document.getElementById("c14").childNodes[0];
	var c15 = document.getElementById("c15").childNodes[0];
	var c16 = document.getElementById("c16").childNodes[0];
	var c17 = document.getElementById("c17").childNodes[0];

	c11.value = 6;
	c12.value = 7;
	c13.value = 8;
	c14.value = 9;
	c15.value = 1;
	c16.value = 2;
	c17.value = 3;
}

function coordinator() {
	var correct = checkIfCorrect();

	if (correct == false) {
		makeNotification("danger", "There was something wrong in your given game");
	} else {
		var results = nextMoves();
		showNextMoves(results);
		console.log(results);
	}

}

function makeNotification(typ, message) {
	var alerts = document.getElementById("alerts");
	var div = document.createElement("div");

	if (typ == "danger") {
		div.setAttribute("class", "alert alert-danger");
	}

	div.setAttribute("role", "alert");
	var node = document.createTextNode(message);
	div.appendChild(node);

	alerts.appendChild(div);

	setTimeout(function () {
		div.remove();
	}, 3000);
}

function checkIfCorrect() {
	return true;
}

function nextMoves() {
	var results = [];

	// Getting all values in a workable Format
	var trRows = document.getElementsByTagName("tr");
	var rows = []; 

	for(var i = 0;i < trRows.length; i ++){
		row = [];
		for(var j = 0; j < trRows[i].cells.length; j++){
			row.push(parseInt(trRows[i].cells[j].childNodes[0].value));
		}
		rows.push(row);
	}
	
	/* Missing one in Horizontal Line */
	var cloneRows = rows.slice();
	results = results.concat(missingInRows(cloneRows));

	/* Missing one in Vertical Line */
	// Rotating Matrix 90°
	var verticalRows = [];
	for(i = 0; i < rows[0].length; i++){
		verticalRows.push([]);
	}

	for(i = 0; i < rows.length; i++){
		for(var j = 0; j < rows[i].length; j++){
			verticalRows[j].push(rows[i][j]);
		}
	}

	// Calculate
	var cloneVerticalRows = verticalRows.slice();
	verticalRowsMissingResults = missingInRows(cloneVerticalRows);

	// Rotate results back
	for(i = 0; i < verticalRowsMissingResults.length; i++){
		verticalRowsMissingResults[i] = [verticalRowsMissingResults[i][1], verticalRowsMissingResults[i][0], verticalRowsMissingResults[i][2]];
	}

	results = results.concat(verticalRowsMissingResults);

	/* Missing in a "Box" (that's what I will be calling the 3x3 parts of Soduko) */

	


	return results;
}

function missingInRows(rows){

	var results = [];

	for (var i = 0; i < rows.length; i++) {
		var NaNSpots = NaNSpotsInList(rows[i]);

		if (NaNSpots.length == 1) {
			var numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9];
			for (var k = numbers.length - 1; k >= 0; k--) {
				if (rows[i].includes(numbers[k])) {
					numbers.splice(k, 1);
				}
			}
			results.push(
				[i, NaNSpots[0], numbers[0]]);
		}
	}

	return results;
}

function NaNSpotsInList(numList) {
	var missingSpots = [];
	for (var l = 0; l < numList.length; l++) {
		if (isNaN(numList[l])) {
			missingSpots.push(l);
		}
	}
	return missingSpots;
}

function showNextMoves(results){
	for(var i = 0; i < results.length; i++){
		var cell = document.getElementById('c' + results[i][0] + results[i][1]).childNodes[0];
		cell.value = results[i][2];
		cell.setAttribute("style", "color: #14a720");
	}
}